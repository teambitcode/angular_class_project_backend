export const USER_ROLE = {
  ADMIN: 'admin',
  AGENT: 'agent',
  CUSTOMER: 'customer'
};

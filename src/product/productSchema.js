const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  description: joi.string().required(),
  price: joi.number().required(),
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  _id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.patch = joi.object().keys({
  name: joi.string(),
  description: joi.string(),
  price: joi.number(),
});

// mainValveStatus registration Schema and validations to be done
module.exports.put = joi.object().keys({
  name: joi.string().required(),
  description: joi.string().required(),
  price: joi.number().required(),
});
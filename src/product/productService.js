const productModel = require('./productModel');
/**
 * find from the system
 */
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        productModel.find(condition).exec((err, product) => {
            err ? reject(err) : resolve(product);
        });
    })
};

/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        productModel.findOneAndDelete({ _id: id },
            async (err, product) => {
                if (err)
                    reject(err);
                else if (product != null || product != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        productModel.findOne({ name: body.name }, async function (err, productData) {
            console.log(productData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (productData == undefined || productData == null) {
                const product = new productModel();
                product.name = body.name;
                product.description = body.description;
                product.price = body.price;

                product.save(async (error, productCb) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(productCb);
                    }
                });
            } else {
                reject("Product ID already exists");
            }
        });
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        locationModel.findOne({ _id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            productModel.findOne({ _id: id }, function (err1, productData) {
                console.log(" in side service");
                console.log(id);
                console.log(productData);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (productData != undefined || productData != null) {
                    productModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("Product ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong");
        }
    })
};
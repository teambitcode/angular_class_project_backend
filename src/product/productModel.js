const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        description: { type: String },
        price : { type: Number },

    }, { timestamps: true }
);
// productSchema.index({ product: "2dsphere" });
module.exports = mongoose.model('products', productSchema);
